$(document).ready(function(){

	$('select').select2({
		width: '100%',
		placeholder: '',
		allowClear: false
	});
	$('#per-pace').rangeslider({
		polyfill: false,
		onInit: function() {
			this.$range.append($('<div class="rangeslider__ruler"><img src="img/ruler.png"> </div>'))
			this.$range.append($('<span class="rangeslider_min" />').html(this.min))
			this.$range.append($('<span class="rangeslider_max" />').html(this.max))
		}
	});
	$('input.range').rangeslider({
		polyfill: false,
	});

	$('form').each(function(){
		if (!$(this).hasClass('ie8')){
			$(this).checkBo();
		}
	});

	var h = 0;
	$('.main-page .white-box').each(function(){
		var h_box = $(this).height();
		if (h_box>=h){
			h=h_box;
			$('.main-page .white-box').height(h)
		} else {
			$('.main-page .white-box').height(h_box)
		}
	});

	$('tbody tr[data-href]').addClass('clickable').click( function() {
		window.location = $(this).attr('data-href');
	});

	new Tablesort(document.getElementById('results-table'));
});